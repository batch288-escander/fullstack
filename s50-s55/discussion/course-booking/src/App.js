import 'bootstrap/dist/css/bootstrap.min.css';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import { useState, useEffect } from 'react';

import { AppNavBar } from './component'
import { Home, Courses, Register, Login, Logout, NoPage, CourseView } from './pages'
import { UserProvider } from './UserContext.js';

import './App.css';

function App() {

  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  })

  const unsetUser = () => {
    localStorage.clear()

    setUser({
      id:null,
      isAdmin: null
    });
  }

  useEffect( () => {
    if(localStorage.getItem('token')) {
      fetch(`${process.env.REACT_APP_API_URL}/users/userDetails`, {
        method: 'GET',
        headers: {
            'Authorization': `Bearer ${localStorage.getItem('token')}`
        }
      }).then(result => result.json())
      .then(data => {
          setUser({
              id: data._id,
              isAdmin: data.isAdmin
          })
      })
      .catch(error => console.log(error))
    }
    
},[])

  return (
    <UserProvider value={{user, setUser, unsetUser}}>
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<AppNavBar/>}>
            <Route index element={<Home />} />
            <Route path="courses" element={<Courses />} />
            <Route path="register" element={<Register />} />
            <Route path="login" element={<Login />} />
            <Route path="logout" element={<Logout />} />
            <Route path="courses/:courseId" element={<CourseView />} />
          </Route>
          <Route path="*" element={<NoPage />} />

        </Routes>
      </BrowserRouter>
    </UserProvider>
  );
}

export default App;