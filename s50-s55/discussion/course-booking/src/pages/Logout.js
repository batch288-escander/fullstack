import { useContext, useEffect } from 'react'
import { Navigate } from 'react-router-dom';

import UserContext from '../UserContext';

const Logout = () => {

  const { setUser, unsetUser } = useContext(UserContext);

  useEffect(() => {
    unsetUser();



  })
  return  (
      <Navigate to='/Login'/>
    )
    
}

export default Logout