import React, {useState, useEffect} from 'react'
import { Container, Row, Col, Card, Button } from 'react-bootstrap'
import { useParams, useNavigate } from 'react-router-dom'
import swal from 'sweetalert';

const CourseView = () => {
    const [courseInfo, setCourseInfo] = useState({name:null,description:null,price:null})

    const { courseId } = useParams();

    const navigate = useNavigate()
    
    useEffect(() => {
        fetch(`${process.env.REACT_APP_API_URL}/courses/${courseId}`)
        .then(result => result.json())
        .then(data => {
            let info = {
                name: data.name,
                description: data.description,
                price: data.price,
            }
            setCourseInfo(info);
        })
    },[courseId])

    const Enroll = (courseId) => {
        fetch(`${process.env.REACT_APP_API_URL}/users/enroll`,{
            method: 'POST',
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('token')}`,
                'Content-type' : 'application/json'
            },
            body: JSON.stringify({
                id: courseId
            })
        })
        .then(result => result.json())
        .then(data => {
            if(data) {
                swal({
                    title: 'Login Successfully',
                    text: 'Redirecting you to homepage...',
                    icon: "success"
                });

                navigate('/courses')

            } else {
                swal({
                    title: 'Something went wrong',
                    text: 'Please try again',
                    icon: "error"
                });

            }
        })
    }


    return (
    <>
        <Container className='mt-5'>
            <Row>
                <Col>
                    <Card >
                        <Card.Body>
                            <Card.Title>{courseInfo.name}</Card.Title>
                            
                            <Card.Subtitle>Description</Card.Subtitle>
                            <Card.Text>{courseInfo.description}</Card.Text>

                            <Card.Subtitle>Price</Card.Subtitle>
                            <Card.Text>{courseInfo.price}</Card.Text>
                            
                            <Card.Subtitle>Class Schedule:</Card.Subtitle>
                            <Card.Text>8am - 5pm</Card.Text>

                            <Button variant="primary" onClick={() => Enroll(courseId)}>Enroll</Button>
                        </Card.Body>
                    </Card>
                </Col>
            </Row>
        </Container>
    </>
  )
}

export default CourseView