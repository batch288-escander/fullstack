import React, { useEffect, useState } from 'react'

import { CourseCard } from '../component'
import coursesData from '../data/courses.js'

const Courses = () => {

  const [courses, setCourses] = useState([]);
  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/courses/active`)
    .then(result => result.json())
    .then(data => {
      setCourses(data);
    })
  },[courses])


  return (
    <>
      <h1 className='text-center mt-3'>Courses</h1>
        {courses.map((course) => (
          <CourseCard courseProps ={course} key = {course.id} />
      ))}
    </>
  )
}

export default Courses