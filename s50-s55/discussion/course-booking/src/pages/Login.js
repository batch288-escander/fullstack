import swal from 'sweetalert';
import React, {useState, useEffect, useContext} from 'react'
import { Container, Row, Col, Form, Button } from 'react-bootstrap'
import { Navigate, Link, useNavigate } from 'react-router-dom'

import UserContext from '../UserContext'

const Login = () => {
    const [isDisabled, setIsDisabled] = useState(true)
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const { user, setUser } = useContext(UserContext)
    
    const navigate = useNavigate()

    const login = (e) => {
        e.preventDefault()
        fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
            method: 'POST',
            headers: {
                'Content-type':'application/json'
            },
            body: JSON.stringify({
                email: email,
                password: password
            })
        }).then(result => result.json())
        .then(data => {
            if(data===false){
                swal({
                    title: 'Login failed',
                    text: 'Please input correct email and password',
                    icon: "error"
                });
            } else {
                swal({
                    title: 'Login Successfully',
                    text: 'Redirecting you to homepage...',
                    icon: "success"
                });
                localStorage.setItem('token', data.auth)
                
                retrieveUserDetails(data.auth)

                navigate('/')
            }
        })
        .catch(error => console.log(error))
    }

    const retrieveUserDetails = (token) => {
        fetch(`${process.env.REACT_APP_API_URL}/users/userDetails`, {
            method: 'GET',
            headers: {
                'Authorization': `Bearer ${token}`
            }
        }).then(result => result.json())
        .then(data => {
            setUser({
                id: data._id,
                isAdmin: data.isAdmin
            })
        })
        .catch(error => console.log(error))
    }

    useEffect(() => {
        if(email && password){   
            setIsDisabled(false) 
        } else {
            setIsDisabled(true)
        }
    }
    ,[email, password])


  return (
    <Container className='mt-5'>
        {user.id && <Navigate to ='/NotAccessible' />}
        <Row>
            <Col className='col-6 mx-auto'>
                <h1 className='text-center'>Login</h1>
                <Form onSubmit={login}>
                    <Form.Group className="mb-3" controlId="formBasicEmail">
                        <Form.Label>Email address</Form.Label>
                        <Form.Control 
                            type="email" 
                            placeholder="Enter email" 
                            value={email}
                            onChange={e => setEmail(e.target.value)} 
                        />
                    </Form.Group>

                    <Form.Group className="mb-3" controlId="formBasicPassword">
                        <Form.Label>Password</Form.Label>
                        <Form.Control 
                            type="password" 
                            placeholder="Password" 
                            value = {password}
                            onChange={e => setPassword(e.target.value)}
                        />
                    </Form.Group>

                    <p>No account yet? <Link to='/register'>Sign up here</Link></p>
                                        
                    <Button 
                        className='mt-3' 
                        variant="primary" 
                        type="submit" 
                        disabled={isDisabled}
                    >
                        Submit
                    </Button>
                </Form>
            </Col>
        </Row>
    </Container>

  )
}
export default Login