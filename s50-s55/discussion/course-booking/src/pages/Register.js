import swal from 'sweetalert';
import React, { useState, useEffect, useContext} from 'react';
import { Container, Row, Col, Form, Button, Alert } from 'react-bootstrap';
import { Link, useNavigate, Navigate } from 'react-router-dom';

import UserContext from '../UserContext';

const Register = () => {
    const [isDisabled, setIsDisabled] = useState(true)
    const [firstName, setFirstName] = useState('')
    const [lastName, setLastName] = useState('')
    const [email, setEmail] = useState('')
    const [mobileNo, setMobileNo] = useState('')
    const [password1, setpassword1] = useState('')
    const [password2, setpassword2] = useState('')
    const { user, setUser } = useContext(UserContext)

    const navigate = useNavigate();

    const register = (e) => {
        e.preventDefault()
        if(password1 === password2){
            fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
                method: 'POST',
                headers: {
                    'Content-type':'application/json'
                },
                body: JSON.stringify({
                    firstName: firstName,
                    lastName: lastName,
                    email: email,
                    password: password1,
                    mobileNo: mobileNo
                })
            }).then(result => result.json())
            .then(data => {
                if(data===false){
                    swal({
                        title: 'Something went wrong',
                        text: 'Please try again',
                        icon: "error"
                    });
                } else {
                    
                    swal({
                        title: 'Welcome to Zuitt',
                        text: 'Please login using your newly created account...',
                        icon: "success"
                    });
                    // login()
                    
                    navigate('/login');
                }
            })
            .catch(error => console.log(error))


        } else {
            swal({
                title: 'Something went wrong',
                text: 'Please try again',
                icon: "error"
            });
        }
    }

    const login = () => {
        fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
            method: 'POST',
            headers: {
                'Content-type':'application/json'
            },
            body: JSON.stringify({
                email: email,
                password: password1
            })
        }).then(result => result.json())
        .then(data => {
            if(data===false){
                swal({
                    title: 'Login failed',
                    text: 'Please input correct email and password',
                    icon: "error"
                });
            } else {
                swal({
                    title: 'Registration Complete!',
                    text: 'Redirecting you to homepage...',
                    icon: "success"
                });
                localStorage.setItem('token', data.auth)
                
                retrieveUserDetails(data.auth)

                navigate('/')
            }
        })
        .catch(error => console.log(error))
    }

    const retrieveUserDetails = (token) => {
        fetch(`${process.env.REACT_APP_API_URL}/users/userDetails`, {
            method: 'GET',
            headers: {
                'Authorization': `Bearer ${token}`
            }
        }).then(result => result.json())
        .then(data => {
            setUser({
                id: data._id,
                isAdmin: data.isAdmin
            })
        })
        .catch(error => console.log(error))
    }

    useEffect(() => {
        console.log(firstName)
        console.log(lastName)
        console.log(mobileNo)
        console.log(email)
        console.log(password1)
        console.log(password2)
        if(firstName && lastName && (mobileNo.length >= 11) && email && password1 && password2){   
            setIsDisabled(false) 
        } else {
            setIsDisabled(true)
        }
    }
    ,[email, password1, password2, firstName, lastName, mobileNo])


  return (
    <Container className='mt-5' >
    <Form   onSubmit={register}>
        {user.id && <Navigate to ='/NotAccessible' />}
        <Row>
            <Col><h1 className='text-center'>Register</h1></Col>
        </Row>

        <Row>
            <Col className='col-md-6 col-12 mx-auto'>
                <Form.Group  className="mb-3" controlId="formBasicFirstName">
                <Form.Label>First Name</Form.Label>
                <Form.Control 
                    type="text" 
                    placeholder="Enter your first name" 
                    value={firstName}
                    onChange={e => setFirstName(e.target.value)} 
                />
                </Form.Group>
            </Col>
            <Col className='col-md-6 col-129 mx-auto'>
                <Form.Group className="mb-3" controlId="formBasicLastName">
                    <Form.Label>Last Name</Form.Label>
                    <Form.Control 
                        type="text" 
                        placeholder="Enter your last name" 
                        value={lastName}
                        onChange={e => setLastName(e.target.value)} 
                    />
                </Form.Group>
            </Col>
        </Row>
        <Row>
            <Col className='col-md-6 col-12 mx-auto'>
                <Form.Group className="mb-3" controlId="formBasicEmail">
                    <Form.Label>Email address</Form.Label>
                    <Form.Control 
                        type="email" 
                        placeholder="Enter email" 
                        value={email}
                        onChange={e => setEmail(e.target.value)} 
                    />
                </Form.Group>
            </Col>
            <Col className='col-md-6 col-12 mx-auto'> 
                <Form.Group className="mb-3" controlId="formBasicContact">
                    <Form.Label>Contact No.</Form.Label>
                    <Form.Control 
                        type="text" 
                        placeholder="Enter contact no." 
                        value={mobileNo}
                        onChange={e => setMobileNo(e.target.value)} 
                    />
                </Form.Group>
            </Col>
        </Row>
        
        <Row>
            <Col className='col-md-6 col-12 mx-auto'>
                <Form.Group className="mb-3" controlId="formBasicPassword1">
                    <Form.Label>Password</Form.Label>
                    <Form.Control 
                        type="password" 
                        placeholder="Password" 
                        value = {password1}
                        onChange={e => setpassword1(e.target.value)}
                    />
                </Form.Group>
            </Col>
            <Col className='col-md-6 col-12 mx-auto'>
                <Form.Group className="mb-3" controlId="formBasicPassword2">
                <Form.Label>Confirm Password</Form.Label>
                <Form.Control 
                    type="password" 
                    placeholder="Retype your nominated password" 
                    value={password2}
                    onChange={e => setpassword2(e.target.value)}
                />
                </Form.Group>
            </Col>


        </Row>
        <Row>
            <Col className='col-12 mx-auto text-center'>
                <p className='mt-3' >Have an account already? <Link to='/login'>Log in here</Link></p>
                <Button 
                    
                    variant="primary" 
                    type="submit" 
                    disabled={isDisabled}
                >
                    Submit
                </Button>
            </Col>
        </Row>
    </Form>
    </Container>
  )
}

export default Register