import React from 'react'

import { Banner, Highlights } from '../component'

const Home = () => {
  return (
    <>
      <Banner />
      <Highlights />
    </>
  )
}

export default Home