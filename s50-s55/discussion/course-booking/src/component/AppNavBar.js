import { useContext } from "react";
import { Outlet, Link, NavLink } from "react-router-dom";
import { Container, Nav, Navbar } from 'react-bootstrap';

import UserContext from "../UserContext";

const AppNavBar = () => {

    const { user } = useContext(UserContext);
    return (
    <>
    <Navbar bg="primary" variant="dark">
      <Container>
        <Navbar.Brand as = {Link} to ="/">Zuitt</Navbar.Brand>
        <Nav className="ms-auto">
          <Nav.Link as = {NavLink} to ="/">Home</Nav.Link>
          <Nav.Link as = {NavLink} to ="/courses">Courses</Nav.Link>
          { !user.id  
            ? 
              <>
                <Nav.Link as = {NavLink} to ="/register">Register</Nav.Link>
                <Nav.Link as = {NavLink} to ="/login">Login</Nav.Link>
              </>
            : 
             <Nav.Link as = {NavLink} to ="/logout">Logout</Nav.Link>
          }
          
        </Nav>
      </Container>
    </Navbar>
    <Outlet />
  </>
  )
}

export default AppNavBar