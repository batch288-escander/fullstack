import React from 'react'
import { Button, Container, Row, Col } from 'react-bootstrap'
import { Link } from 'react-router-dom'

const Banner = () => {
  return (
    <Container>
        <Row>
            <Col className='mt-2 text-center'>
                <h1>Zuitt Coding Bootcamp</h1>
                <p>Opportunities for everyone, everywhere!</p>
                <Button as ={Link} to="/courses">Enroll Now!</Button>
            </Col>
        </Row>
    </Container>
  )
}

export default Banner