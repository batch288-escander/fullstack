// import AppNavBar from './AppNavBar.js'
// import Banner from './Banner.js'
// import Highlights from './Highlights.js'
// import CourseCard from './CourseCard.js'

// export  {AppNavBar, Banner, Highlights, CourseCard}

export { default as AppNavBar } from './AppNavBar.js'
export { default as Banner } from './Banner.js'
export { default as Highlights } from './Highlights.js'
export { default as CourseCard } from './CourseCard.js'
