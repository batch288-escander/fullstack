import React, {useState, useEffect, useContext } from 'react'
import { Container, Row, Col, Card, Button, Alert } from 'react-bootstrap'
import { Link } from 'react-router-dom'

import UserContext from '../UserContext'

const CourseCard = (props) => {
    const [count, setcount] = useState(0)
    const [seats, setseats] = useState(30)
    const [isDisabled, setisDisabled] = useState(false)
    const { user } = useContext(UserContext)

    const { _id, name, description, price } = props.courseProps
    
    const enroll = () => {
        setcount(count+1)
        setseats(seats-1)
    }


    useEffect(() => {
        if(seats === 0){
            setisDisabled(true)
        }
    }, [seats])

  return (
    <Container className='mt-4' >
        <Row>
            <Col>
                <Card className="cardHighLight" >
                    <Card.Body>                                            
                        <Card.Title>{name}</Card.Title>

                        <Card.Subtitle>Description:</Card.Subtitle>
                        <Card.Text>{description}</Card.Text>

                        <Card.Subtitle>Price:</Card.Subtitle>
                        <Card.Text>Php {price}</Card.Text>

                        <Card.Subtitle>Enrollees:</Card.Subtitle>
                        <Card.Text>{count}</Card.Text>
                        
                        {seats===0 && 
                        (
                            <Alert variant='warning'>No more seats available!</Alert>
                        )
                        }
                        { user
                        ? <Button variant="primary" as = {Link} to ={`/courses/${_id}`}>Course Details</Button>
                        : <Button variant="primary" as ={Link} to ='/login' >Login to enroll</Button>    

                        }
                    </Card.Body>
                </Card>
            </Col>
        </Row>
    </Container>
  )
}

export default CourseCard